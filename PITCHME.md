---
theme: base-theme
style: |
  @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed');
  section {

    font-family: 'Roboto Condensed', sans-serif;
  }
---


<style>

@import-theme 'base';


section {
	font-family: 'Roboto Condensed';
}


section.lead h1 {
  text-align: center;
  vertical-align: bottom;
  padding: 0px;
  color: green;
  font-family: 'Roboto Condensed', sans-serif;

}

section.imc img {
  display: block;
  margin-left: auto;
  margin-right: auto;
  _filter: grayscale(0.8) blur(1px);
}
section.imc h1 {
  text-align: center;
}

section.imc p {
  padding: 50px;
  text-align: center;
}

</style>

<!--

_class: imc
_footer: '[geomio.bitbucket.io](https://geomio.bitbucket.io)'
-->

# <!--fit--> New advances in creating and testing initial configurations of thermomechanical simulations with 
![width:400px](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/geomIO_logo.png)


Tobias Baumann & Arthur Bauville 


---

<!--
header: 'Updated repository ([bitbucket.org/geomio/geomio](https://bitbucket.org/geomio/geomio)) & **new wiki pages** (https://bitbucket.org/geomio/geomio/wiki/Home)'
footer: '![width:75px opacity:.5](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/geomIO_logo.png)'
paginate: true
-->


![ bg  80%](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/geomiO_bitbucket.png)

![bg  85%](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/geomio_wiki.png)

---

<!-- 
header: ' Motivation'
-->

# How to create a 3D model geometry for thermomechanical simulations?

It should incorporate
1. **geophysical constraints** (grids, points, ...)
2. **geological interpretations** (cross-sections, maps,...)

![bg right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/3D_Tibet.png   )

---

# Commercial software exists, but ...

- expensive
- steep learning curve
- lack of flexibility (i.e. API to own thermomechanical code)

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/977a39df9c03c9c681d2754a1a8fca7ee6df30a9/pics/geomodeller.png)

---

<!-- 
header: ' geomIO workflow'
-->

# geomIO workflow
![](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/geomIO_workflow.png)

---

# Usually, we start with collecting data 

We merge all data that we find in **maps**, and create orthogonal **cross-sections**

The **generic mapping toolbox** is a great tool for doing that 
![width:200px](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/gmt_logo.png) 

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/tibet3d_map.png)

---

# Interpreting the data
We use the vectorgraphics editor (**Inkscape**) to interpret the data on orthogonal **cross-sections**


![width:200px](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/is_logo.png) 

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/3D_section.png)


---

<!-- header: '3D example' -->

# Example 
## Gorleben salt structure

Structures were created with data from maps-servers  (NIBIS and geotis) that allow you to visualize 2D cross-sections

![width:300px](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/ngermanysalt.png)

![bg right fit](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/SaltStructure.png)

---

<!-- 
header: 'Creating a 3D structure from 2D cross-sections'
-->

# Adding geometry constraints

1. Add a hidden layer and name it as `#_<layername>`
2. Drag and drop  image on the Inkscape canvas
![bg right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/is_tools/01_ImportData.gif)

---

# Defining the scale of the model

1. Add a `Reference` layer with a Bezier line object
2. Associate the object with an attribute `CoordRef` and coordinates as values `0,-3500,2000,-2000`


![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/is_tools/02_AddReference_smaller.gif)

---

# Start interpreting

The layername specifies the orientation (`EW`,`NS`,`HZ`) & coordinate normal to the drawing layer

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/is_tools/03_FirstLayer_smaller.gif)

---

# Adding a second layer

The layername is adapted such that it defines the offset of the layers

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/is_tools/05_Add2ndLayer_smaller.gif)

---

# Adding a second layer

... we duplicate <kbd>ctrl+d</kbd> and modify the object in the new layer.

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/is_tools/06_ModifyObjectIn2ndLayer_smaller.gif)

---

<!-- header: '3D example: rabbit-and-cat tutorial (/tutorials/ Tuto3D_Basic)' -->

# 3D Rabbit-and-cat tutorial

We provide several tutorials in `/tutorials`. This one illustrates well how geomIO works.


![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/Fig_S1.png)

---

<!-- header: '3D example: rabbit-and-cat tutorial (/tutorials/ Tuto3D_Basic)' -->

# geomIO user code

``` Matlab
clc, clear, close all
%opt                 = geomIO_Options();
opt.DrawCoordRes    = 10;
opt.inputFileName   = ['Input/RabbitAndCat.HZ.svg'];
opt.writeParaview   = true;
opt.shiftPVobj      = [0 0 1];

opt.readLaMEM           = true;
opt.NewLaMEM            = false;
opt.LaMEMinputFileName  = './Input/RabbitAndCat.dat';
opt.writePolygons       = true;
[~, Volumes]            = run_geomIO(opt);

plotVolumes(Volumes,{'CatAndRabbit'})
```

![bg right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/tutopics/3dtuto.gif)

---

<!-- 
header: 'Initial temperature configuration (tutorials/Example3D_Geodynamics) :new:'
-->

# Basic temperature configuration

## Simplified plate model

- **Input:** SVG file with horizontal (`HZ`) contours of oceanic and continental lithosphere
- geomIO associates 1D temperature profiles to each geological unit

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/Fig_S2.png)


---

<!-- 
header: 'Initial temperature configuration (tutorials/Example3D_Slab_temperature) :new:'
-->

# Advanced temperature configuration

## Japanese subduction zones

- **Input:** Slab surface geometries (slab contours as `HZ`.SVG file) + age grid (e.g., M�ller et al.)
- Half-space cooling model (depth dependent)
- Approx. Voronoi diagram for efficient distance-to-surface calculations

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/TemperatureJapan.png)

---

<!-- 
header: 'Gravity modelling with geomIO (/tutorials/Example2D_Gravity/) :new:'
-->

# 2D Gravity model of salt structures

- Method: Talwani et al. (1959)
- Works with any 2D geomIO object

![width:300px](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/2Dsignal.png)

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/2D_diapirsection.png)


---

<!-- 
header: 'Gravity modelling with geomIO (/tutorials/Benchmark3D_gravity/) :new: '
-->

# 3D Gravity modelling

- Method: Talwani & Ewing (1960) ![width:300px](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/Talwani.png)
- works with any 3D geomIO object  

![bg fit right](https://bitbucket.org/geomio/geomio_basics/raw/962d9b5fb63d46f55ca73787da5c1bc64aaa5c79/pics/3dgravity2.png)


---

<!-- 
header: Summary  
footer: Thanks @marp-team for providing [![width:70px](https://github.com/marp-team/marp/blob/master/marp.png?raw=true)](https://marp.app/)
-->

# Summary

- geomIO repository now contains a detailed wiki :orange_book: & tutorials :mortar_board:
- paper in press (G3 technical report)
- Initial temperature conditions :new:
- 2D & 3D Gravity modelling to test interpretations :new: